package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/gianfebrian/dhooker/cache"
)

const (
	retryInterval = 200 * time.Millisecond
	retryTimeout  = 5 * time.Minute

	smsParserTplKey = "sms-parser-template"
)

type hjson map[string]interface{}

type nexmoSMSHookPayload struct {
	MSISDN           string `json:"msisdn"`
	To               string `json:"to"`
	MessageID        string `json:"messageId"`
	Text             string `json:"text"`
	Type             string `json:"type"`
	Keyword          string `json:"keyword"`
	MessageTimestamp string `json:"message-timestamp"`
}

var registry *cache.Cache

func init() {
	registry = cache.New(cache.NoExpiration, cache.NoExpiration)
}

func main() {
	http.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())

		token := strconv.FormatInt(time.Now().Unix(), 10)
		registry.Set(token, nil, cache.NoExpiration)

		header := w.Header()
		header.Add("content-type", "application/json")
		fmt.Fprintf(w, rjson(hjson{"token": token}))
	})

	http.HandleFunc("/hook/", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())

		header := w.Header()
		header.Add("content-type", "application/json")

		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": "Invalid HTTP method"}))

			return
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": err.Error()}))

			return
		}

		registry.Set(r.URL.Path[len("/hook/"):], b, cache.NoExpiration)

		if len(b) < 1 {
			fmt.Fprintf(w, rjson(hjson{"status": true, "no-data": true}))

			return
		}

		fmt.Fprintf(w, string(b))
	})

	http.HandleFunc("/clear/", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())

		header := w.Header()
		header.Add("content-type", "application/json")

		registry.Delete(r.URL.Path[len("/clear/"):])
		w.WriteHeader(http.StatusOK)
	})

	http.HandleFunc("/subscribe/", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())

		header := w.Header()
		header.Add("content-type", "application/json")

		b := retry(registry, r.URL.Path[len("/subscribe/"):], retryInterval, 0, retryTimeout)

		q := r.URL.Query()
		tpl := q.Get("tpl")
		if tpl != "" {
			b = parseTemplate(string(b), tpl)
		}

		fmt.Fprintf(w, string(b))
	})

	http.HandleFunc("/webhook/sms-parser", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())

		header := w.Header()
		header.Add("content-type", "application/json")

		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": "Invalid HTTP method"}))

			return
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": err.Error()}))

			return
		}

		var payload struct {
			Name     string `json:"name"`
			Template string `json:"template"`
		}

		if err := json.Unmarshal(b, &payload); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": err.Error()}))

			return
		}

		registry.Set(fmt.Sprintf("%v-%v", smsParserTplKey, payload.Name), payload.Template, cache.NoExpiration)

		w.WriteHeader(http.StatusNoContent)
	})

	http.HandleFunc("/webhook/inbound-sms", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())

		header := w.Header()
		header.Add("content-type", "application/json")

		if r.Method != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": "Invalid HTTP method"}))

			return
		}

		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": err.Error()}))

			return
		}

		if len(b) < 1 {
			fmt.Fprintf(w, rjson(hjson{"status": true, "no-data": true}))

			return
		}

		payload := new(nexmoSMSHookPayload)
		if err := json.Unmarshal(b, payload); err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprintf(w, rjson(hjson{"error": err.Error()}))

			return
		}

		registry.Set(payload.To, []byte(payload.Text), cache.NoExpiration)

		w.WriteHeader(http.StatusNoContent)
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("APP_PORT")), nil))
}

func retry(reg *cache.Cache, key string, interval, cur, timeout time.Duration) []byte {
	d, ok := reg.Get(key)
	if ok {
		if d != nil {
			return d.([]byte)
		}
	}

	time.Sleep(interval)

	cur += interval

	if cur.Nanoseconds() >= timeout.Nanoseconds() {
		return nil
	}

	return retry(reg, key, interval, cur, timeout)
}

func removeRedundantWhiteChar(s string) string {
	return strings.Join(strings.Fields(s), " ")
}

func parseTemplate(text, tplName string) []byte {
	content := strings.Split(removeRedundantWhiteChar(text), " ")

	tpl, ok := registry.Get(fmt.Sprintf("%v-%v", smsParserTplKey, tplName))
	if !ok {
		tpl = ""
	}

	tplContent := strings.Split(tpl.(string), " ")

	out := make(map[string]string)

	for i, t := range tplContent {
		if strings.Contains(t, "{{") && strings.Contains(t, "}}") {
			key := strings.Replace(t, "{{", "", -1)
			key = strings.Replace(key, "}}", "", -1)
			out[key] = content[i]
		}
	}

	b, _ := json.Marshal(out)

	return b
}

func rjson(h hjson) string {
	out, _ := json.Marshal(h)

	return string(out)
}
