FROM golang:1.9-alpine3.7 AS builder

WORKDIR $GOPATH/src/gitlab.com/gianfebrian/dhooker

ADD . .

RUN CGO_ENABLED=0 go install -ldflags '-extldflags "-static"'

FROM scratch

COPY --from=builder /go/bin/dhooker /usr/bin/dhooker

ENTRYPOINT [ "dhooker" ]